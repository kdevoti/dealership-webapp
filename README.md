# CarCar
​
Team:
​
* Kacton Devoti -- Service
* Silvano Gonzalez -- Sales
​
## How to Run this App
- This App is made using the following requirements:
    -Docker
    -Node.js
    -Git
    **Also available is Insomnia to add backend data for testing APIs.
    1. Fork this repository
    ​
    2. Clone the forked repository onto your local computer:
    git clone "repositoryURL"
    ​
    3. Build and run the project using Docker with these commands:

        docker volume create beta-data
        docker-compose build
        docker-compose up

    - After running these commands, make sure all of your Docker containers are running
    ​
    - View the project in the browser: http://localhost:3000/
    ​
## Design
- The CarCar project is designed to handle inventory management, sales and service operations for a car dealership. It is comprised of microservices based on the mentioned services. Our project relies on RESTful API calls. React was used to in creating the front-end and the back-end was created using Django. The database used is PostgreSQL.

## Diagram
-Image:  project-beta/images/CarCar.png

​
### URLs and Ports
- Inventory Microservice
    http://localhost:8100

- Sales Microservice
    http://localhost:8090

- Service Microservice
    http://localhost:8080

​
### Service API

For the service microservies, the AutomobileVO, Technician, and Appointment models were all created. In the Appointment model, a foreign key was utilized for both the Technician and AutomobileVO properties. In order to make an appointment you need to fill out a form that included the two foreign keys. The AutomobileVO which implemented a poller to retrieve Automobile information from the Inventory microservice, and the Technician that assigned a technician to the appointment.

This micrservice took 7 different View functions, 5 of which pertained to the Appointment model. These 5 functions enabled the client to see a list of appointments, create an appointment, see specific appointments, delete a specific appointment, cancel and modify an appointment, finish an appointment, and open an appointment.

### Service Endpoints

| Feature          | Method          | URL          |
|------------------|-----------------|--------------|

# Technicians
|List Technicians| GET |http://localhost:8080/api/technicians/|
|Show Technician| GET |http://localhost:8080/api/technicians/id/|

# Appoinments
|Create Service appointment| POST |http://localhost:8080/api/appointments/|
|List Appointments| GET |http://localhost:8080/api/appointments/|
|View Appointment| GET |http://localhost:8080/api/appointment/id/|
|Cancel Appointment| GET |http://localhost:8080/api/appointment/${id}/cancel|
|Finish Appointment| GET |http://localhost:8080/api/appointment/${id}/finish|
|Open Appointment| GET |http://localhost:8080/api/appointment/${id}/open|
|View Service History| GET |http://localhost:8080/api/appointments/|

​
### Sales API
The Sales microservice was comprised of 4 models: Salesperson, Customer, AutomobileVO, and Sale. The Sale model had three foreign keys, receiving data from the other 3 models. These models along with a price property were used to create and record sales on the CarCar application, giving the client the ability keep track of their sales, salespeople, customers, and sold vehicles in their inventory.

The AutomobileVO model was updated straight from the Inventory microservice through a method called polling. A poller was set up to check Inventory for new automobiles. If a vehicle was added, the poller will update the Vin and Sold properties in the AutomobileVO model. Making it usable in the Sale model. For a sale to created, there must be at least one of each item in the other models. You need a Salesperson, Customer, and AutomobileVO(pulled straight from the inventory).


### Sales Endpoints

# Salesperson

|  Action  |  Method  | URL
|----------|----------|-----------|
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Salesperson details | GET | http://localhost:8090/api/salesperson/id/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/id/


# Customer
|  Action  |  Method  | URL
|----------|----------|-----------|
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Show a specific customer | GET | http://localhost:8090/api/customers/id/

# Sale


|  Action  |  Method  | URL
|----------|----------|-----------|
| List all salesrecords | GET | http://localhost:8090/api/salesrecords/
| Create a new sale | POST | http://localhost:8090/api/salesrecords/


​
