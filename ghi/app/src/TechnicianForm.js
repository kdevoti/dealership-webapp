import React, { useState } from "react";

function TechnicianForm(props) {
  const [employee_id, setEmployeeID] = useState("");
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");

  function handleChangeEmployeeID(event) {
    setEmployeeID(event.target.value);
  }

  function handleChangeFirstName(event) {
    setFirstName(event.target.value);
  }

  function handleChangeLastName(event) {
    setLastName(event.target.value);
  }

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      employee_id,
      first_name,
      last_name,
    };

    const technicianUrl = "http://localhost:8080/api/technicians/";
    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(technicianUrl, fetchOptions);
    if (response.ok) {
      await response.json();
      setEmployeeID("");
      setFirstName("");
      setLastName("");
    }
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-technician-form">
                <h1 className="card-title">Add a Technician</h1>
                <div className="form-floating mb-3">
                  <input
                    value={employee_id}
                    onChange={handleChangeEmployeeID}
                    required
                    placeholder="Employee ID"
                    type="text"
                    id="employee_id"
                    className="form-control"
                  />
                  <label htmlFor="employee_id">Employee ID</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={first_name}
                    onChange={handleChangeFirstName}
                    required
                    placeholder="First Name "
                    type="text"
                    id="first_name"
                    name="first_name"
                    className="form-control"
                  />
                  <label htmlFor="first_name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={last_name}
                    onChange={handleChangeLastName}
                    required
                    placeholder="Last Name "
                    type="text"
                    id="last_name"
                    name="last_name"
                    className="form-control"
                  />
                  <label htmlFor="last_name">Last Name</label>
                </div>
                <button className="btn btn-lg btn-primary">Add</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TechnicianForm;
