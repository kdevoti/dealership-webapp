import React, { useState, useEffect, useCallback } from "react";

function AppointmentList(props) {
  const [appointments, setAppointments] = useState([]);

  const handleCancel = async (id) => {
    const response = await fetch(
      `http://localhost:8080/api/appointment/${id}/cancel`,
      { method: "PUT" }
    );
    const data = await response.json();
    console.log(data);
    props.getAppointments();
  };

  const handleFinish = async (id) => {
    const response = await fetch(
      `http://localhost:8080/api/appointment/${id}/finish`,
      { method: "PUT" }
    );
    const data = await response.json();
    console.log(data);
    props.getAppointments();
  };

  useEffect(() => {
    async function getApptsDetails() {
      // fetch data
      try {
        const requests = [];
        const appts = [];
        const openAppts = props.appointments.filter(
          (appt) => appt.status === "open"
        );
        openAppts.forEach((appt) => {
          requests.push(
            fetch(`http://localhost:8080/api/appointment/${appt.id}/`).then(
              async (resp) => {
                const appt = await resp.json();
                appts.push(appt);
              }
            )
          );
        });
        await Promise.all(requests);
        setAppointments(appts);
      } catch (err) {
        console.error(err);
      }
    }

    getApptsDetails();
  }, [props.appointments]);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Is VIP?</th>
          <th>Customer</th>
          <th>Date</th>
          <th>Time</th>
          <th>Technician</th>
          <th>Reason</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {appointments.map((appointment) => {
          const appoinmentDate = new Date(appointment.date_time);
          const formattedDate = appoinmentDate.toLocaleDateString();
          const formattedTime = appoinmentDate.toLocaleTimeString();
          return (
            <tr key={appointment.vin}>
              <td>{appointment.vin}</td>
              <td>{appointment.vip ? "Yes" : "No"}</td>
              <td>{appointment.customer}</td>
              <td>{formattedDate}</td>
              <td>{formattedTime}</td>
              <td>{appointment.technician.employee_id}</td>
              <td>{appointment.reason}</td>
              <td>
                <button
                  className="btn btn-danger"
                  onClick={() => handleCancel(appointment.id)}
                >
                  Cancel
                </button>
              </td>
              <td>
                <button
                  className="btn btn-success"
                  onClick={() => handleFinish(appointment.id)}
                >
                  Finish
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AppointmentList;
