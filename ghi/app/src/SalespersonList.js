import React, { useEffect, useState } from "react";

function SalespersonList(props) {
  const [salespeople, setList] = useState([]);

  useEffect(() => {
    fetchSalespeople();
  }, []);

  const fetchSalespeople = async () => {
    const url = "http://localhost:8090/api/salespeople/";

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data.salesperson);
      setList(data.salesperson);
    }
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        {salespeople.map((salesperson) => {
          return (
            <tr key={salesperson.employee_id}>
              <td>
                {salesperson.first_name} {salesperson.last_name}
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default SalespersonList;
