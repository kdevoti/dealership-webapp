import React, { useState } from 'react';

function ManufacturerForm(props) {
    const [name, setName] = useState('');

    function handleChangeName(event) {
        setName(event.target.value);
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            name,
        };

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(manufacturerUrl, fetchOptions);
        if (response.ok) {
            await response.json();
            setName('');
        }
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-manufacturer-form">
                                <h1 className="card-title">Add a Manufacturer</h1>
                                <div className="form-floating mb-3">
                                    <input value={name} onChange={handleChangeName} required placeholder="Manufacturer Name" type="text" id="name" className="form-control" />
                                    <label htmlFor="employee_id">Manufacturer Name</label>
                                </div>
                                <button className="btn btn-lg btn-primary">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default ManufacturerForm;
